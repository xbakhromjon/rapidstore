package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"slices"
	"strconv"
	"time"
)

const (
	CRLF = "\r\n"
	PONG = "PONG"
	OK   = "+OK\r\n"
	NIL  = "$-1\r\n"
	PORT = "6379"
)

var (
	PX   = []byte("px")
	PING = []byte("ping")
	ECHO = []byte("echo")
	GET  = []byte("get")
	SET  = []byte("set")
)

var cache = make(map[string]Value)

type Value struct {
	Data      string
	ExpiresAt *time.Time
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:"+PORT)
	if err != nil {
		fmt.Println("Failed to bind to port ", PORT)
		os.Exit(1)
	}

	for {
		c, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting connection: ", err.Error())
			continue
		}
		fmt.Println("Accepted new connection from ", c.RemoteAddr())
		go func() {
			if err := handleConnection(c); err != nil {
				switch {
				case errors.Is(err, io.EOF):
					fmt.Println("Connection closed")
				default:
					fmt.Println("Error handling connection: ", err.Error())
				}
			}
		}()
	}
}

func handleConnection(c net.Conn) error {
	defer func() {
		err := c.Close()
		if err != nil {
			fmt.Println("Error closing connection: ", err.Error())
		}
	}()
	for {
		buf := make([]byte, 128)
		_, err := c.Read(buf)
		if err != nil {
			return fmt.Errorf("error reading from connection: %w", err)
		}

		parts := bytes.Split(buf, []byte(CRLF))
		count, err := strconv.Atoi(string(bytes.TrimPrefix(parts[0], []byte("*"))))

		parts = parts[1:]
		args := make([][]byte, 0, 10)
		j := 1
		for i := 0; i < count; i++ {
			args = append(args, parts[j])
			j += 2
		}
		command := bytes.ToLower(args[0])
		switch {
		case slices.Equal(command, PING):
			err = pingHandler(c)
		case slices.Equal(command, ECHO):
			err = echoHandler(c, args)
		case slices.Equal(command, SET):
			err = setHandler(c, args)
		case slices.Equal(command, GET):
			err = getHandler(c, args)
		default:
			err = errors.New("unsupported command")
		}
		if err != nil {
			return err
		}
	}
}

func respWriter(c io.Writer, msg string) error {
	_, err := c.Write([]byte(msg))
	if err != nil {
		return fmt.Errorf("error writing to connection: %w", err)
	}
	return nil
}

func pingHandler(c net.Conn) error {
	return respWriter(c, fmt.Sprintf("+%s\r\n", PONG))
}

func echoHandler(c net.Conn, args [][]byte) error {
	return respWriter(c, fmt.Sprintf("$%d\r\n%s\r\n", len(args[1]), args[1]))
}

func setHandler(c net.Conn, args [][]byte) error {
	if len(args) < 3 {
		return fmt.Errorf("expected 3 arguments, got %d", len(args))
	}
	key := args[1]
	valData := args[2]
	val := Value{Data: string(valData)}
	if len(args) > 4 && bytes.Equal(bytes.ToLower(args[3]), PX) {
		ttl, err := strconv.Atoi(string(args[4]))
		if err != nil {
			return fmt.Errorf("error parsing ttl: %w", err)
		}
		expiresAt := time.Now().Add(time.Duration(ttl) * time.Millisecond)
		val.ExpiresAt = &expiresAt
	}
	cache[string(key)] = val
	return respWriter(c, OK)
}

func getHandler(c net.Conn, args [][]byte) error {
	if len(args) < 2 {
		return fmt.Errorf("expected 2 arguments, got %d", len(args))
	}
	key := args[1]
	val := cache[string(key)]
	if len(val.Data) == 0 {
		return respWriter(c, NIL)
	} else {
		fmt.Println("ExpiresAt: ", val.ExpiresAt)
		if val.ExpiresAt != nil && val.ExpiresAt.Before(time.Now()) {
			delete(cache, string(key))
			return respWriter(c, NIL)
		} else {
			return respWriter(c, fmt.Sprintf("$%d\r\n%s\r\n", len(val.Data), val.Data))
		}
	}
}
