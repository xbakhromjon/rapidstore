package main

import (
	"net"
	"testing"
)

// Example
//  17121	     61299 ns/op

func BenchmarkEcho(b *testing.B) {
	for i := 0; i < b.N; i++ {
		conn, err := net.Dial("tcp", "0.0.0.0:6379")
		if err != nil {
			b.Errorf("Error connection to server: %s\n", err)
		}

		_, err = conn.Write([]byte("*2\r\n$4\r\necho\r\n$3\r\nhey\r\n"))
		if err != nil {
			b.Errorf("Error writing to server: %s\n", err)
		}
		resp := make([]byte, 0)
		_, err = conn.Read(resp)
		if err != nil {
			b.Errorf("Error reading from connection: %s\n", err)
		}
		err = conn.Close()
		if err != nil {
			b.Errorf("Error closing connection")
		}
	}
}
