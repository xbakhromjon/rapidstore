# rapidstore

RapidStore: A High-Performance, Lightweight In-Memory Data Store
RapidStore is a blazing-fast in-memory data store built from the ground up in Go. Inspired by Redis, RapidStore delivers
essential features for caching, real-time data processing, and microservices communication, all within a streamlined and
memory-efficient design.

## Contents

- [Supported operations](#supported-operations)
- [Installation](#installation)
- [Examples](#examples)
- [Authors](#authors)

## Supported operations

- Ping (health check)
- Echo
- Set
- Get

## Installation

```bash
git clone https://gitlab.com/xbakhromjon/rapidstore.git
cd rapidstore
go run main.go
```

## Examples

#### Ping

```
redis-cli -p 6379 ping 
```
Output: `PONG`

#### Echo

```
redis-cli -p 6379 echo hi
```
Output: `hi`

#### Set

```
redis-cli -p 6379 set foo bar
```

with TTL in ms
```
redis-cli -p 6379 set foo bar px 1000
```

Output: `OK`

#### Get

```
redis-cli -p 6379 get foo
```

Output: `bar`

## Authors

- [xbakhromjon](https://github.com/xbakhromjon) - Original author and architect.